package org.r78.em;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Класс реализующий интерфейс IDataStorage и обеспечивающий хранение данных
 * в файле на жестком диске
 */
public class DataStorage implements IDataStorage {

    private final String fileName;

    public DataStorage(String fileName) {
        this.fileName = fileName + ".txt";
    }

    /**
     * Сохраняет список сотрудников в файл
     * @param employees список сотрудников
     */
    @Override
    public void save(@NotNull List<? extends Employee> employees) {
        try (PrintWriter pw = new PrintWriter(fileName)) {
            int id = 1;
            for  (Employee e : employees) {
                pw.println(e.getId());
                pw.println(e.getName());
                pw.println(e.getStatus());
            }
        }
        catch(IOException exc) {
            System.out.println("Ошибка ввода-вывода: " + exc);
        }
    }


    /**
     * Загружает сотрудников из файла
     * @return список сотрудников
     */
    @Override
    public List<Employee> load() {
        List<Employee> staff = new ArrayList<>();
        try (Scanner in = new Scanner(new File(fileName))) {
            Integer ID = 0;
            while (in.hasNextLine()) {
                ID = Integer.parseInt(in.nextLine());
                Employee e = new Employee(in.nextLine(), in.nextLine());
                e.setId(ID);
                staff.add(e);
            }
            EmployeeRepository.setID(ID + 1);
        }
        catch(IOException exc) {
            System.out.println("Ошибка ввода-вывода: " + exc);
        }
        return staff;
    }
}
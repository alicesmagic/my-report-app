package org.r78.em;

/**
 * Класс формирующий список сотрудников. Позволяет добавлять новых сотрудников, удалять
 * уволившихся, просматривать данные одного сотрудника, или всех сразу,
 * присваивать уникальные строковые описания каждому сотруднику, отправлять
 * отчеты о присутствии сотрудников на рабочем месте.
 */
class StaffList {
    private String defaultStatus = "нет данных...";
    private final IEmployeeRepository repository;
    private IUserInterface ui = new UserInterface();

    StaffList (IEmployeeRepository repository) {
        this.repository = repository;
    }

    /**
     * Этот метод реализует главное меню программы
     */
    void start() throws NotIdException, RuntimeException {
        String  choice = "";
        do {
            this.ui.showMenu();
            choice = this.ui.userInput("Введите номер нужного действия и нажмите <ENTER>:\n" +
                    "(Для выхода из программы просто нажмите клавишу <ENTER>)");
            switch (choice) {
                case "1": this.addEmployees(); break;
                case "2": this.removeEmployees(); break;
                case "3": this.showEmployee(); break;
                case "4": this.editEmployee(); break;
                case "5": this.showAllEmployees(); break;
                case "6": this.markAllEmployees(); break;
                case "7": this.resetMarks(); break;
                case "8":
                    System.out.println("Извините! Отправка отчета " +
                            "пока не реализована.");
                    break;
            }
        } while (!choice.isEmpty());
    }

    /**
     * Этот метод добавляет сотрудника в список
     */
    private void addEmployees() {
        System.out.println("ДОБАВЛЕНИЕ В СПИСОК");
        int sizeOld = repository.getAll().size();
        String inp;
        while (!(inp = this.ui.userInput("Введите фамилию и имя нового сотрудника, " +
                        "или нажмите клавишу <ENTER> для выхода в меню:")).isEmpty()) {
            if (inp.length() > 0) {
                this.repository.saveEmployee(new Employee(inp, defaultStatus));
                this.ui.showConfirmMessage("Сотрудник добавлен");
            }
        }
        System.out.println("Добавлено новых сотрудников: " +
                (this.repository.getAll().size() - sizeOld) + " чел.");
        System.out.println("Текущая численность персонала: " +
                this.repository.getAll().size() + " чел.");
        backToMenu();
    }

    /**
     * Этот метод удаляет сотрудников из списка
     */
    private void removeEmployees() {
        System.out.println("УДАЛЕНИЕ ИЗ СПИСКА");
        int sizeOld = this.repository.getAll().size();
        for (Employee e : this.repository.getAll()) {
            System.out.println(e.getId() + " " + e.getName());
        }
        String inp;
        while (!(inp = this.ui.userInput("Введите id удаляемого сотрудника, " +
                        "или нажмите клавишу <ENTER> для выхода в меню:")).isEmpty()) {
            int id;
            try {
                id = Integer.parseInt(inp);
                this.repository.removeById(id);
                this.ui.showConfirmMessage("Сотрудник удален");
            } catch (NumberFormatException exc) {
                this.ui.showErrorMessage("Id может быть только целым числом!");
            } catch (NotIdException exc) {
                System.out.println(exc);
            }
        }
        System.out.println("Удалено сотрудников: " +
                (sizeOld - this.repository.getAll().size()) + " чел.");
        System.out.println("Текущая численность персонала: " +
                this.repository.getAll().size() + " чел.");
        backToMenu();
    }

    /**
     * Этот метод позволяет просмотреть данные выбранного сотрудника
     */
    private void showEmployee() {
        System.out.println("ПРОСМОТР ДАННЫХ СОТРУДНИКА");
        for (Employee e : this.repository.getAll()) {
            System.out.println(e.getId() + " " + e.getName());
        }
        String inp;
        while (!(inp = this.ui.userInput("Введите id сотрудника, " +
                        "или нажмите клавишу <ENTER> для выхода в меню:")).isEmpty()) {
            try {
                Employee e = repository.findById(Integer.parseInt(inp));
                if (e != null) {
                    System.out.println("Данные запрашиваемого сотрудника:");
                    this.ui.showOneEmployee(e);
                }
                else {
                    throw new NotIdException();
                }
            } catch (NumberFormatException exc) {
                this.ui.showErrorMessage("Id может быть только целым числом!");
            } catch (NotIdException exc) {
                System.out.println(exc.getMessage());
            }
        }
    }

    /**
     * Этот метод позволяет редактировать данные выбранного сотурдника
     */
    private void editEmployee() {
        System.out.println("РЕДАКТИРОВАНИЕ ДАННЫХ СОТРУДНИКА");
        for (Employee e : this.repository.getAll()) {
            System.out.println(e.getId() + " " + e.getName());
        }
        String inp;
        while (!(inp = this.ui.userInput("Введите id сотрудника, " +
                        "или нажмите клавишу <ENTER> для выхода в меню:")).isEmpty()) {
            try {
                Employee e = repository.findById(Integer.parseInt(inp));
                if (e != null) {
                    System.out.println("Редактирование сотрудника:");
                    this.ui.showOneEmployee(e);
                    String oldName = e.getName();
                    e.setName(this.ui.userInput("Введите новое имя сотрудника, " +
                            "или нажмите <ENTER>, чтобы пропустить:"));
                    if (e.getName().isEmpty()) {
                        e.setName(oldName);
                    }
                    e.setStatus(this.ui.userInput("Введите новый статус сотрудника, " +
                            "или нажмите <ENTER>, чтобы пропустить:"));
                    if (e.getStatus().isEmpty()) {
                        e.setStatus(defaultStatus);
                    }
                    this.ui.showConfirmMessage("Данные сотрудника отредактированы");
                    this.repository.saveEmployee(e);
                }
                else {
                    throw new NotIdException();
                }
            } catch (NumberFormatException exc) {
                this.ui.showErrorMessage("Id может быть только целым числом!");
            } catch (NotIdException exc) {
                System.out.println(exc);
            }
        }
    }

    /**
     * Этот метод выводит список всех сотрудников
     */
    private void showAllEmployees() {
        System.out.println("ПРОСМОТР СПИСКА ВСЕХ СОТРУДНИКОВ");
        this.ui.showAllEmployees(this.repository.getAll());
        if (this.repository.getAll().size() == 0) {
            this.ui.showErrorMessage("Список пуст!");
        }
        backToMenu();
    }

    /**
     * Этот метод позволяет внести изменения в поле Status всех сотурдников
     */
    private void markAllEmployees() {
        System.out.println("МАРКИРОВКА ПРИСУТСТВИЯ НА РАБОЧЕМ МЕСТЕ");
        for (Employee e : this.repository.getAll()) {
            e.setStatus(this.ui.userInput(e.getName() + ": "));
            if (e.getStatus().isEmpty()) {
                e.setStatus(defaultStatus);
            }
            this.repository.saveEmployee(e);
        }
        backToMenu();
    }

    /**
     * Этот метод сбрасывает поле Status всех сотрудников к значению по умолчанию
     */
    private void resetMarks() {
        System.out.println("МАРКИРОВКА ПРИСУТСТВИЯ СБРОШЕНА!");
        for (Employee e : this.repository.getAll()) {
            e.setStatus(defaultStatus);
            this.repository.saveEmployee(e);
        }
        backToMenu();
    }

    /**
     * Этот метод ставит работу программы на паузу перед возвратом в меню
     */
    private void backToMenu() {
        while (!this.ui.userInput("Для возврата в меню нажмите клавишу <ENTER>:").isEmpty());
    }
}
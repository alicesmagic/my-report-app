package org.r78.em;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Scanner;


public class UserInterface implements IUserInterface {
    private String inputUser;

    UserInterface() {

    }

    /**
     * Метод выводит на экран главное меню
     */
    @Override
    public void showMenu() {
        System.out.println("МЕНЮ:");
        System.out.println("1. Добавить нового сотрудника");
        System.out.println("2. Удалить сотрудника");
        System.out.println("3. Показать сотрудника");
        System.out.println("4. Редактировать данные сотрудника");
        System.out.println("5. Показать всех сотрудников");
        System.out.println("6. Отметить присутствие");
        System.out.println("7. Сбросить отметки о присутствии");
        System.out.println("8. Отправить отчет");
    }

    /**
     * Метод запрашивает ввод пользователя
     * @param message Строка приглашения к вводу
     * @return данные введенные пользователем
     */
    @Override
    public String userInput(String message) {
        System.out.println(message);
        Scanner in = new Scanner(System.in);
        return in.nextLine();
    }

    @Override
    public void showOneEmployee(Employee employee) {
        System.out.println(employee.getId() + " " + employee.getName()
                + " " + employee.getStatus());
    }

    @Override
    public void showConfirmMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void showErrorMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void showAllEmployees(@NotNull List<? extends Employee> employees) {
        int maxLength = 0;
        for  (Employee e : employees) {
            if (maxLength < e.getNameLength())
                maxLength = e.getNameLength();
        }
        for  (Employee e : employees) {
            System.out.printf("%s. %-" + maxLength + "s\t%s\n",
                    e.getId(), e.getName(), e.getStatus());
        }
    }
}
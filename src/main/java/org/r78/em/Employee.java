package org.r78.em;

/**
 * Класс данных описывающих сотрудника
 */
public class Employee {

    private String name;
    private String status;
    private int nameLength;
    private Integer id;

    Employee(String name, String status) {
        this.name = name;
        this.status = status;
        nameLength = name.length();
    }

    /**
     * Этот метод возвращает имя сотрудника
     * @return имя сотрудника
     */
    String getName() {
        return name;
    }

    /**
     * Этот метод позволяет отредактировать имя сотрудника
     * @param name Новое имя
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Этот метод возвращает статус (уникальное строковое описание) сотрудника
     * @return статус сотрудника
     */
    String getStatus() {
        return status;
    }

    /**
     * Этот метод позволяет установить статус (уникальное строковое описание)
     * сотрудника
     * @param status Уникальное строковое описание
     */
    void setStatus(String status) {
        this.status = status;
    }

    /**
     * Этот метод возвращает порядковых номер сотрудника в списке
     * @return порядковый номер в списке
     */
    Integer getId() {
        return id;
    }

    /**
     * Этот метод устанавливает уникальный id сотруднику
     */
    void setId(Integer id) {
        this.id = id;
    }

    /**
     * ВРЕМЕННЫЙ. ИСПОЛЬЗУЕТСЯ, ПОКА НЕ УДАЛЕН КЛАСС org.r78.em.StaffList
     * Этот метод возвращает полную длину имени сотрудника.
     * Необходим для формирования удобочитаемого табулированного списка
     * @return полная длина имени сотрудника
     */
    int getNameLength() {
        return nameLength;
    }
}
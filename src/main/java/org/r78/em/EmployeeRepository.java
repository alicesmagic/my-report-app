package org.r78.em;

import java.util.List;

public class EmployeeRepository implements IEmployeeRepository {
    private List<Employee> staff;
    private final IDataStorage dataStorage;
    private static Integer ID;

    public EmployeeRepository(IDataStorage dataStorage) {
        this.dataStorage = dataStorage;
        ID = 1;
        staff = this.dataStorage.load();
    }

    /**
     * Статический метод установки ID
     * @param ID - устанавливаемый ID сотрудника
     */
    public static void setID(Integer ID) {
        EmployeeRepository.ID = ID;
    }

    /**
     * Метод обеспечивает поиск сотрудника в репозитории по Id
     * @param id - Id искомого сотрудника
     * @return - данные сотрудника
     */
    @Override
    public Employee findById(Integer id) throws RuntimeException {
        if (id == null) {
            throw new RuntimeException("Id can't be null");
        }
        for  (Employee e : staff) {
            if (e.getId().equals(id)) {
                return e;
            }
        }
        return null;
    }

    /**
     * Метод удаляет сотрудника из репозитория по Id
     * @param id - Id удаляемого сотрудника
     */
    @Override
    public void removeById(Integer id) throws NotIdException, RuntimeException {
        if (id == null) {
            throw new RuntimeException("Id can't be null");
        }
        int index = -1;
        for (int i = 0; i < staff.size(); i++) {
            if (staff.get(i).getId().equals(id)) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            staff.remove(index);
            this.dataStorage.save(staff);
        }
        else {
            throw new NotIdException();
        }
    }
    /**
     * Метод предоставляет полный список сотрудников в репозитории
     * @return - коллекция сотрудников
     */
    @Override
    public List<Employee> getAll() {
        return staff;
    }

    /**
     * Метод сохраняет сотрудника в репозитории
     * @param employee - сотрудник, подготовленный к сохранению в репозитории
     * @return - сохраненный сотрудник
     */
    @Override
    public Employee saveEmployee(Employee employee) {
        if (employee.getId() == null) {
            staff.add(employee);
            employee.setId(ID++);
        }
        this.dataStorage.save(staff);
        return employee;
    }
}
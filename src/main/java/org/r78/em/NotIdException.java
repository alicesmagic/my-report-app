package org.r78.em;

/**
 * Created by Alice on 1:13 23.09.2019.
 */
public class NotIdException extends Exception {
    public String toString() {
        return "Этот Id не используется!";
    }
}
package org.r78.em

/**
 * Этот интерфейс обеспечивает хранение данных
 */
interface IEmployeeRepository {

    /**
     * Метод предоставляет полный список сотрудников в репозитории
     * @return - коллекция сотрудников
     */
    val all: List<Employee>

    /**
     * Метод обеспечивает поиск сотрудника в репозитории по Id
     * @param id - Id искомого сотрудника
     * @return - данные сотрудника
     */
    @Throws(RuntimeException::class)
    fun findById(id: Int?): Employee

    /**
     * Метод удаляет сотрудника из репозитория по Id
     * @param id - Id удаляемого сотрудника
     */
    @Throws(NotIdException::class, RuntimeException::class)
    fun removeById(id: Int?)

    /**
     * Метод сохраняет сотрудника в репозитории
     * @param employee - сотрудник, подготовленный к сохранению в репозитории
     * @return - сохраненный сотрудник
     */
    fun saveEmployee(employee: Employee): Employee
}
package org.r78.em

/**
 * Этот интерфейс обеспечивает взаимодействие пользователя с программой.
 * Обображает информацию и принимает пользовательский ввод
 */
interface IUserInterface {
    /**
     * Метод выводит на экран главное меню
     */
    fun showMenu()

    /**
     * Метод запрашивает ввод пользователя
     * @param message Строка приглашения к вводу
     * @return данные введенные пользователем
     */
    fun userInput(message: String): String

    /**
     * Выводит на экран список сотрудников
     * @param employees список сотрудников
     */
    fun showAllEmployees(employees: List<Employee>)

    /**
     * Выводит на экран информацию о пользователе
     * @param employee
     */
    fun showOneEmployee(employee: Employee)

    /**
     * Выводит на экран сообщение об удачном завершении действия
     * @param message сообщение которое нужно вывести
     */
    fun showConfirmMessage(message: String)

    /**
     * Выводит на экран сообщение об ошибек при выполнении действия
     * @param message сообщение которое нужно вывести
     */
    fun showErrorMessage(message: String)

}
package org.r78.em

/**
 * Этот интерфейс обеспечивает хранение данных сотрудников
 */
interface IDataStorage {

    /**
     * Сохраняет список сотрудников в файл
     * @param employees список сотрудников
     */
    fun save(employees: List<Employee>)

    /**
     * Загружает сотрудников из файла
     * @return список сотрудников
     */
    fun load(): List<Employee>
}

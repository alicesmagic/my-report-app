package org.r78.em

class EmployeeApplication

fun main() {
    val dataStorage = DataStorage("department_Java")
    val employeeRepository = EmployeeRepository(dataStorage)
    val staffList = StaffList(employeeRepository)
    staffList.start()
}

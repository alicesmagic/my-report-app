package org.r78.em;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class IEmployeeRepositoryTest {

    private IEmployeeRepository employeeRepository;

    @Before
    public void initial() {
        final Employee employee = new Employee("Hello", "World");
        this.employeeRepository = new EmployeeRepository(null);
        this.employeeRepository.saveEmployee(employee);
    }

    @Test
    public void editEmployee() {
        List<Employee> employees = this.employeeRepository.getAll();
        Employee employee = employees.get(0);
        final Integer expectedId = employee.getId();
        employee.setName("Vasya");
        Employee actual = this.employeeRepository.saveEmployee(employee);
        assertEquals(expectedId, actual.getId());
        assertEquals(1, this.employeeRepository.getAll().size());

    }

    @Test
    public void findByIdSuccessful() throws NotIdException {
        final Employee actual = this.employeeRepository.findById(1);
        assertNotNull(actual);
        assertEquals(actual.getName(), "Hello");
        assertEquals(actual.getStatus(), "World");
    }

    @Test
    public void findByIdFailure() throws NotIdException {
        final Employee actual = this.employeeRepository.findById(2);
        assertNull(actual);
    }

    @Test (expected = RuntimeException.class)
    public void findByIdFailureWithNull() throws RuntimeException,
            NotIdException {
        this.employeeRepository.findById(null);
    }

    @Test
    public void removeByIdSuccessful() throws NotIdException {
        this.employeeRepository.removeById(1);
        assertEquals(this.employeeRepository.getAll().size(), 0);
    }

    @Test
    public void removeByIdSuccessfulWithNewObjectID() throws NotIdException {
        this.employeeRepository.removeById(new Integer(1));
        assertEquals(this.employeeRepository.getAll().size(), 0);
    }

    @Test (expected = RuntimeException.class)
    public void removeByIdFailureWithNull() throws RuntimeException,
            NotIdException {
        this.employeeRepository.removeById(null);
    }

    @Test (expected = NotIdException.class)
    public void removeByIdFailure() throws NotIdException {
        this.employeeRepository.removeById(1);
        this.employeeRepository.removeById(1);
    }

    @Test
    public void getAll() {
        List<Employee> actual = this.employeeRepository.getAll();
        assertNotNull(actual);
        assertEquals(actual.size(), 1);
    }

    @Test
    public void save() {
        Employee employee = new Employee("world", "hello");
        assertNull(employee.getId());
        Employee actual = this.employeeRepository.saveEmployee(employee);
        assertNotNull(actual.getId());
        assertEquals(this.employeeRepository.getAll().size(), 2);
    }
}